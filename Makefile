.PHONY: all download_d3js download_mermaid download_remark download_3Dmol 

all: download_d3js download_remark download_mermaid download_3Dmol

download_d3js:
	wget -O public/wiring/d3.v5.js https://d3js.org/d3.v5.js 

download_remark:
	wget -O public/wiring/remark-latest.min.js https://remarkjs.com/downloads/remark-latest.min.js

download_mermaid:
	wget -O public/wiring/mermaid.min.js https://cdn.jsdelivr.net/npm/mermaid/dist/mermaid.min.js

download_3Dmol:
	wget -O public/wiring/3Dmol-min.js https://3Dmol.csb.pitt.edu/build/3Dmol-min.js

#update_angularplasmid:
#	git fetch angularplasmid master
#	git subtree pull --prefix public/wiring/angularplasmid angularplasmid master --squash


#update_revealjs:
#	git fetch revealjs master
#	git subtree pull --prefix public/wiring/reveal.js revealjs master --squash

#update_dagrejs:
#	git fetch dagrejs master
#	git subtree pull --prefix public/wiring/dagre.js dagrejs master --squash

#update_reveald3:
#	git fetch reveald3 master                                              
#	git subtree pull --prefix public/wiring/reveald3 reveald3 master --squash

